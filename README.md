Dew Point for Android
=====================

(c) 2008-2021 by Markus Hoffmann

<img alt="Logo" src="fastlane/metadata/android/en-US/images/icon.png" width="120" />

Calculates the Dew Point.

This app calculates the Dew Point based on temperature and relative humidity.
Tap the thermometers and simply adjust the values for relative air humidity and
air temperature. The dew point and the absolute water content of the air are
calculated automatically. The dew point tells you, at which temperature of e.g.
surfaces, walls or window panes condensation starts.  So: Walls which have a
lower temperature than what the dew point tells you, get wet! Window panes get
fogged. Make sure, that humidity is sufficient low and the walls are adequately
warm.

Taupunkt f�r Android
=====================

(c) 2008-2021 by Markus Hoffmann

Rechnet den Taupunkt aus.

Dieses kleine Programm erlaubt es Ihnen, den Taupunkt auszurechnen. Hierzu
k�nnen Sie durch Antippen die relative Luftfeuchtigkeit und die Lufttemperatur
eingeben. Taupunkt und Wassergehalt der Luft werden automatisch berechnet. Der
Taupunkt besagt, bei welcher Temperatur z.B. von Oberfl�chen, W�nden oder
Fensterscheiben die Kondensation einsetzt. Also: W�nde, welche eine Temperatur
kleiner der des Taupunkts haben, werden na�! Scheiben beschlagen. Achten Sie
also immer darauf, da� die Luftfeuchtigkeit hinreichend klein, und die W�nde
ausreichend warm sind.


Point de Ros�e
==============

(c) 2008-2021 by Markus Hoffmann

Calcule le point de ros�e.


Cette application calcule le point de ros�e bas� sur la temp�rature et
l'humidit� relative.  Appuyez sur les thermom�tres et il suffit de r�gler les
valeurs de l'humidit� relative de  l'air et de la temp�rature de l'air. Le point
de ros�e et la teneur en eau absolue de l'air sont  calcul�s automatiquement.

Le point de ros�e vous indique � quelle temp�rature, par exemple de surfaces,
des murs ou des vitres condensation commence.  Donc: murs qui ont une
temp�rature inf�rieure � ce que le point de ros�e vous indique, se mouiller! Les
vitres vont se embu�. Assurez-vous que l\'humidit� est suffisante bas et les
murs sont suffisamment chaud.

### Download

[<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/app/de.hoffmannsgimmickstaupunkt)

### Screenshots

<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/de-DE/images/phoneScreenshots/1.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/de-DE/images/phoneScreenshots/2.png" width="30%">
</div>

<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" width="24%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" width="24%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" width="24%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/4.png" width="24%">
</div>

### Important Note:

No software can be perfect. We do our best to keep this app bug free, 
improve it and fix all known errors as quick as possible. 
However, this program is distributed in the hope that it will 
be useful, but WITHOUT ANY WARRANTY; without even the implied 
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
Use this program on your own risk. 
Please report all errors, so we can fix them. 


    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; Version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


Building the .apk file
======================

The build uses the gradle environment. A ./gradlew build should do.

 git clone git@codeberg.org:kollo/Taupunkt.git

then do a 
  cd Taupunkt
  ./gradlew build
(Enter passwords for the keystore)
(the compile process will take a while.)

The apk should finally be in build/outputs/apk/Taupunkt-release.apk
