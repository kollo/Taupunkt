package de.hoffmannsgimmickstaupunkt;

/* AboutActivity.java (c) 2008-2015 by Markus Hoffmann 
 *
 * This file is part of Taupunkt / Dew Point for Android 
 * ==================================================================
 * Taupunkt / Dew Point for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;


public class AboutActivity extends Activity {
	
    private TextView readme;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);
        readme=(TextView)findViewById(R.id.description);
        readme.setText(Html.fromHtml(getResources().getString(R.string.readme)+
        		getResources().getString(R.string.news)+getResources().getString(R.string.impressum)
        		));
    }
    @Override
	public void onStart() {
		super.onStart();
		SharedPreferences settings =PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        int munit = settings.getInt("munit", 0);
    }
}
