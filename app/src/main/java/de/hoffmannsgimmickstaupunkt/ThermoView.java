package de.hoffmannsgimmickstaupunkt;

/* ThermoView.java (c) 2008-2015 by Markus Hoffmann 
 *
 * This file is part of Taupunkt / Dew Point for Android 
 * ==================================================================
 * Taupunkt / Dew Point for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;


public class ThermoView extends View  implements OnTouchListener  {
    
	private Paint paint;
	  
	private int textHeight;
	private int bx,by,bw,bh;
	private int sx,sy,sw,sh;
	private float tempmin=30, tempmax=-10;
	private float tmin=-10, tmax=45;
	private int tendenz;
	public float temperature;
	
	public void setTemperature(float t) {
		if(t-temperature>0) tendenz=1;
		else if(t-temperature<0) tendenz=-1;
		else tendenz=0;
		temperature=t;
		if(temperature>tempmax) tempmax=temperature;
    	if(temperature<tempmin) tempmin=temperature;
	}
	public void setMinmax(float t1,float t2) {
		 tmin=t1;
		 tmax=t2;
	}
	public void resetMinmax(float t1,float t2) {
		 tempmin=tmax;
		 tempmax=tmin;
	}
    public ThermoView(Context context) {
        super(context);
        initThermoView();        
    }
    public ThermoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initThermoView();  
      }
    public ThermoView(Context context, AttributeSet attrs, int defaultStyle) {
        super(context, attrs, defaultStyle);
        initThermoView();  
      }
    protected void initThermoView() {
    	setFocusable(true); //necessary for getting the touch events
        setFocusableInTouchMode(true);
        this.setOnTouchListener(this);
       
        paint=new Paint();
      //  paint.setStrokeWidth(1);
      //  paint.setStyle(Paint.Style.STROKE);
    	paint.setAntiAlias(false);
        paint.setStyle(Style.FILL_AND_STROKE);
        paint.setColor(Color.BLUE);
        paint.setTextSize(20);
      }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    	int modex=MeasureSpec.getMode(widthMeasureSpec);
    	int sizex=MeasureSpec.getSize(widthMeasureSpec);
    	int modey=MeasureSpec.getMode(heightMeasureSpec);
    	int sizey=MeasureSpec.getSize(heightMeasureSpec);
    	if (modex == MeasureSpec.UNSPECIFIED) { 
    		sw=200;
    	} else {
    		sw=sizex;
    	}
    	if (modey == MeasureSpec.UNSPECIFIED) { 
    		sh=280;
    	} else {
    		sh=sizey;
    	}
    	setMeasuredDimension(sw, sh);
    }
  	private int kt(double dut) {
		  return by+bh-(int)((dut-tmin)/(tmax-tmin)*bh);
	}
  	

    
    @Override
    public void onDraw(Canvas canvas) {
    	sw=getMeasuredWidth();
        sh=getMeasuredHeight();
        bw=Math.min(sw,sh/2);
        float tf=(float) (sw/120.0);
        bh=sh-7-13-14;
        by=sy+7;
        bx=sx+(sw-bw)/2;
        
        canvas.drawColor(Color.BLACK);
        paint.setTextSize(16*tf);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.WHITE);
      //  paint.setAntiAlias(true);
        canvas.drawCircle(bx+bw/2,by,6, paint);
        canvas.drawCircle(bx+bw/2,sy+sh-14,14, paint);
      //  paint.setAntiAlias(false);
        canvas.drawLine(bx+bw/2-6,kt(tmin),bx+bw/2-6,kt(tmax), paint);
        canvas.drawLine(bx+bw/2+6,kt(tmin),bx+bw/2+6,kt(tmax), paint);
               
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        
        paint.setColor(Color.BLACK);
        canvas.drawRect(bx+bw/2-5,by,bx+bw/2+6,by+bh, paint);
        
        if(temperature<0) paint.setColor(Color.BLUE);
        else paint.setColor(Color.RED);
        
        canvas.drawCircle(bx+bw/2,sy+sh-14,13, paint);
        canvas.drawRect(bx+bw/2-5,kt(temperature),bx+bw/2+6,by+bh, paint);
  	  paint.setColor(Color.WHITE);

  	  canvas.drawLine(bx+bw/2-6,kt(tempmin),bx+bw/2+7,kt(tempmin), paint);
  	  canvas.drawLine(bx+bw/2-6,kt(tempmax),bx+bw/2+7,kt(tempmax), paint);
  	  if(Math.signum(tmin)!=Math.signum(tmax)) canvas.drawLine(bx+bw/2-10,kt(0),bx+bw/2+12,kt(0), paint);

  	  canvas.drawLine(bx+bw/2+20,by,bx+bw/2+20,by+bh, paint);
  	  float tstep=1;
  	  String s;
  	  paint.setTextSize(6*tf);
  	  float tstart,tend;
  	  int x;
  	  if(TaupunktActivity.munit==0) {
  	 	  tstart=Math.max(0,tmin);
  	  	  tend=tmax*(float)1.01;  
  	  } else {
  	 	  tstart=Math.max(0,(float)c2f(tmin));
  	  	  tend=(float)c2f(tmax)*(float)1.01;  
  	  }
  	  for(float i=tstart; i<=tend;i+=tstep) {
  		  if(TaupunktActivity.munit==0) x=kt(i);
  		  else x=kt(f2c(i));
  		  if((int)i%10==0) {
  			  canvas.drawLine(bx+bw/2+20,x,bx+bw/2+20+6,x, paint);
  			  s=String.format("%d",(int) i);
  			  canvas.drawText(s,bx+bw/2+20+8,x+3*tf,paint);
  		  }
  		  else if((int)i%5==0) canvas.drawLine(bx+bw/2+20,x,bx+bw/2+20+4,x, paint);
  		  else canvas.drawLine(bx+bw/2+20,x,bx+bw/2+20+2,x, paint);
  	  }
  	  if(TaupunktActivity.munit==0) {
  	 	  tstart=Math.min(0,tmax);
  	  	  tend=tmin;  
  	  } else {
  	 	  tstart=Math.min(0,(float)c2f(tmax));
  	  	  tend=(float)c2f(tmin);  
  	  }
  	  for(float i=Math.min(0,tmax); i>=tmin;i-=tstep) {
  		  if(TaupunktActivity.munit==0) x=kt(i);
  		  else x=kt(f2c(i));
  
 		  if((int)i%10==0) {
 			  canvas.drawLine(bx+bw/2+20,x,bx+bw/2+20+6,x, paint);
 			  s=String.format("%d",(int) i);
  			  canvas.drawText(s,bx+bw/2+20+8,x+3*tf,paint);
 		  }
  		  else if((int)i%5==0) canvas.drawLine(bx+bw/2+20,x,bx+bw/2+20+4,x, paint);
  		  else canvas.drawLine(bx+bw/2+20,x,bx+bw/2+20+2,x, paint);
 	  
  	  }
  	paint.setAntiAlias(true);
  	paint.setTextSize(16*tf);
  	if(TaupunktActivity.munit==0) s=""+Math.round(temperature*10)/10.0+"°C";
  	else s=""+Math.round(c2f(temperature)*10)/10.0+"°F";
  	canvas.drawText(s,bx+bw/2-8-paint.measureText(s),by+bh/2,paint);
  	paint.setAntiAlias(false);
  }
    
    private double c2f(double c) {
        return c*9/5+32;	
    }
    private double f2c(double f) {
    	return (f-32)*5/9;
    }
    
    public boolean onTouch(View view, MotionEvent event)
    {
    	float nt=tmin+(by+bh-event.getY())/bh*(tmax-tmin);
    	if(nt>tmax) nt=tmax;
      setTemperature(nt);
      invalidate();
      return true;
    }
}
