package de.hoffmannsgimmickstaupunkt;

/* PreferencesActivity.java (c) 2008-2015 by Markus Hoffmann 
 *
 * This file is part of Taupunkt / Dew Point for Android 
 * ==================================================================
 * Taupunkt / Dew Point for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;

public class PreferencesActivity extends PreferenceActivity  implements OnSharedPreferenceChangeListener {
  private final static String HOMEPAGE="https://codeberg.org/kollo/Taupunkt";
  private final static String LICENSE_PAGE="https://codeberg.org/kollo/Taupunkt/raw/branch/master/LICENSE";
  private final static String SOURCE_PAGE="https://codeberg.org/kollo/Taupunkt";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    addPreferencesFromResource(R.xml.preferences);
    findPreference("about_version").setSummary(applicationVersion());
    findPreference("about_homepage").setSummary(HOMEPAGE);
    for(int i=0;i<getPreferenceScreen().getPreferenceCount();i++) {
        initSummary(getPreferenceScreen().getPreference(i));
    }	
  }
  
  @Override
  protected void onResume() {
    super.onResume();
     // Set up a listener whenever a key changes	     
    getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
  }

  @Override
  protected void onPause() {
    super.onPause();
    // Unregister the listener whenever a key changes 	    
    getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);	 
  }
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) { 
    updatePrefSummary(findPreference(key));
    TaupunktActivity.setunit(Integer.valueOf(sharedPreferences.getString("select_unit", "0")));
    boolean a=sharedPreferences.getBoolean("expert_mode", false);
    if(a) TaupunktActivity.setmode(1);
    else TaupunktActivity.setmode(0);
    float p=(float)1013.25;
    try {p= (float) (Double.parseDouble(sharedPreferences.getString("select_pressure", "1013.25")));} catch(NumberFormatException nfe) {}
    TaupunktActivity.setpressure(p);
    float tmin=-10,tmax=45,hmin=0,hmax=100;
    try {tmin= (float) (Double.parseDouble(sharedPreferences.getString("select_tmin", "-15")));} catch(NumberFormatException nfe) {}
    try {tmax= (float) (Double.parseDouble(sharedPreferences.getString("select_tmax", "50")));} catch(NumberFormatException nfe) {}
    try {hmin= (float) (Double.parseDouble(sharedPreferences.getString("select_hmin", "0")));} catch(NumberFormatException nfe) {}
    try {hmax= (float) (Double.parseDouble(sharedPreferences.getString("select_hmax", "100")));} catch(NumberFormatException nfe) {}
    TaupunktActivity.setscaleminmax(tmin,tmax,hmin,hmax);
  }

  private void initSummary(Preference p) {
    if(p instanceof PreferenceCategory) {
      PreferenceCategory pCat = (PreferenceCategory)p;
      for(int i=0;i<pCat.getPreferenceCount();i++) initSummary(pCat.getPreference(i));
    } else updatePrefSummary(p);
  }

  private void updatePrefSummary(Preference p){
    if(p instanceof ListPreference) {
      ListPreference listPref = (ListPreference) p; 
      p.setSummary(listPref.getEntry()); 
    }
    if(p instanceof EditTextPreference) {
      EditTextPreference editTextPref = (EditTextPreference) p; 
      p.setSummary(editTextPref.getText()); 
    }
  }

  private final String applicationVersion() {
    try {return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;}
    catch (NameNotFoundException x)  {return "unknown";}
  }

  @Override
  public boolean onPreferenceTreeClick(final PreferenceScreen preferenceScreen, final Preference preference)  {
    final String key = preference.getKey();
    if("about_license".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(LICENSE_PAGE)));
      finish();
    } else if("about_homepage".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(HOMEPAGE)));
      finish();
    } else if ("about_version".equals(key)) {
      startActivity(new Intent(PreferencesActivity.this, AboutActivity.class));
      finish();
    } else if("about_source".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SOURCE_PAGE)));
      finish();
    } else if ("about_market_app".equals(key)) {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("market://details?id=%s", getPackageName()))));
      finish();
    } else if ("about_market_publisher".equals(key))  {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:\"Markus Hoffmann\"")));
      finish();
    }
    return false;
  }
}
