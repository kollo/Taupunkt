package de.hoffmannsgimmickstaupunkt;

/* TaupunktActivity.java (c) 2008-2015 by Markus Hoffmann 
 *
 * This file is part of Taupunkt / Dew Point for Android 
 * ==================================================================
 * Taupunkt / Dew Point for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TaupunktActivity extends Activity {
	static ThermoView thermo;
	static HygroView hygro;
	static TextView taupunkt,wasser,zeile1,zeile2,zeile3;
	static EditText editpressure,edittemp,edithumidity;
	static LinearLayout textfelder;
	static String abk;
	public static int munit=0; /*0 celsius, 1 fahrenheit */
	private static int mexpert=0;  /*0 normal mode, 1 expert mode */
	
	static float pressure=(float)1013.25;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences settings =PreferenceManager.getDefaultSharedPreferences(this);
        boolean do_expert=settings.getBoolean("expert_mode", false);
        if(do_expert) mexpert=1;
        else mexpert=0;
		try {pressure= (float) (Double.parseDouble(settings.getString("select_pressure", "1013.25")));} catch(NumberFormatException nfe) {}
		munit=Integer.valueOf(settings.getString("select_unit","0"));
        float tmin=-10,tmax=45,hmin=0,hmax=100;
        try {tmin= (float) (Double.parseDouble(settings.getString("select_tmin", "-15")));} catch(NumberFormatException nfe) {}
        try {tmax= (float) (Double.parseDouble(settings.getString("select_tmax", "50")));} catch(NumberFormatException nfe) {}
        try {hmin= (float) (Double.parseDouble(settings.getString("select_hmin", "0")));} catch(NumberFormatException nfe) {}
        try {hmax= (float) (Double.parseDouble(settings.getString("select_hmax", "100")));} catch(NumberFormatException nfe) {}

		setContentView(R.layout.main);
        taupunkt=(TextView) this.findViewById(R.id.taupunkt);
        wasser=(TextView) this.findViewById(R.id.wasser);
        textfelder=(LinearLayout) this.findViewById(R.id.textfelder);
        editpressure=(EditText) this.findViewById(R.id.editpressure);
        edittemp=(EditText) this.findViewById(R.id.edittemp);
        edithumidity=(EditText) this.findViewById(R.id.edithumidity);
        zeile1=(TextView) this.findViewById(R.id.zeile1);
        zeile2=(TextView) this.findViewById(R.id.zeile2);
        zeile3=(TextView) this.findViewById(R.id.zeile3);
        
        thermo = (ThermoView) this.findViewById(R.id.thermometer);
        thermo.setTemperature(18);
        thermo.setTemperature(22);
        thermo.setTemperature(20);
        thermo.setMinmax(tmin,tmax);
        
        hygro = (HygroView) this.findViewById(R.id.hygrometer);
        hygro.setTemperature(40);
        hygro.setTemperature(60);
        hygro.setTemperature(50);
        hygro.setMinmax(hmin,hmax);
 
        abk=getResources().getString(R.string.abk);
        
        
        edittemp.setText(""+thermo.temperature);
        edithumidity.setText(""+hygro.temperature);
        hygro.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				boolean ret=hygro.onTouch(v,event);
				edithumidity.setText(""+Math.round(hygro.temperature*10)/10.0);
				redraw();
				return ret;
			}
        });
        thermo.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				boolean ret=thermo.onTouch(v,event); 
				edittemp.setText(""+Math.round(thermo.temperature*10)/10.0);

				redraw();
				return ret;
			}
        });
        editpressure.setText(""+pressure);
        editpressure.addTextChangedListener(new TextWatcher(){
        	
        	public void onTextChanged(CharSequence s, int start, int before, int count) {}
           	public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
       	
        	public void afterTextChanged(Editable s) {
        		if(s.length()>0) pressure=(float) Double.parseDouble(s.toString());
        		redraw();
        	}
        });
       edittemp.addTextChangedListener(new TextWatcher(){
        	
        	public void onTextChanged(CharSequence s, int start, int before, int count) {}
           	public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
       	
        	public void afterTextChanged(Editable s) {
        		if(s.length()>0) {
        			try {thermo.setTemperature((float)Double.parseDouble(s.toString()));}
        			catch (NumberFormatException nfe) {}
        		}
        		redraw();
        	}
        });
       edithumidity.addTextChangedListener(new TextWatcher(){
       	
       	public void onTextChanged(CharSequence s, int start, int before, int count) {}
          	public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
      	
       	public void afterTextChanged(Editable s) {
       		if(s.length()>0) hygro.setTemperature((float)Double.parseDouble(s.toString()));
       		redraw();
       	}
       });
       setmode(mexpert);
       
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	Intent viewIntent;
        switch (item.getItemId()) {
            case R.id.infotext:    
            	viewIntent = new Intent(TaupunktActivity.this, AboutActivity.class);
            	startActivity(viewIntent);
            	break;
            case R.id.settings:     
            	viewIntent = new Intent(TaupunktActivity.this, PreferencesActivity.class);
            	startActivity(viewIntent);
            	break;
            case R.id.finish: 
            	finish();
                break;
            default: 
            	return super.onOptionsItemSelected(item);
        }
        return true;
    }
    private static double c2f(double c) {
      return c*9/5+32;		
    }
    public static void setunit(int m) {
      munit=m;
      redraw();
    }
    public static void setmode(int m) {
    	mexpert=m;
       if(mexpert==0) {
    	  textfelder.setVisibility(View.GONE);
    	  
       } else {
    	  textfelder.setVisibility(View.VISIBLE);
       }
       redraw();
    }
    public static void setpressure(float m) {
    	pressure=m;
        editpressure.setText(""+pressure);
		redraw();
    }
    public static void setscaleminmax(float t1,float t2,float h1,float h2) {
    	thermo.setMinmax(t1,t2);
    	hygro.setMinmax(h1,h2);
		redraw();
    }
    
    private static String t2fs(double c) {
    	if(munit==0) return(""+Math.round(c*10)/10.0+"°C");
    	else return(""+Math.round(c2f(c)*10)/10.0+"°F");	
    }
    private static void redraw() {
    	float T=thermo.temperature;  /* in Deg Celsius */
    	float r=hygro.temperature;   /* in percent */
    	float a,b;    /* Konstanten fuer Saettigungsdampfdruck Formel*/
    	if(T>=0) {
    		a=(float) 7.5;    /*for use in vapor pressure */
    		b=(float) 237.3;  /*with respect to WATER     */
    	} else {
    		a=(float) 7.6;
    		b=(float) 240.7;
    	//	a=(float) 9.5;    /*for use in vapor pressure */
    	//	b=(float) 265.5;  /*with respect to ICE       */
    	}
    	float sdd=(float) (6.1078*Math.pow(10,((a*T)/(b+T)))); /*saturation vapour pressure in hPa*/

    	float dd=r/100*sdd;  /* vapour pressure in hPa */
    	float v=(float) Math.log10(dd/6.1078);
    	float td=b*v/(a-v);  /* dew point */

    	float mw=(float) 18.016;
    	float Rs=(float) 8314.3;
    	float TK=(float) (T+273.15);
    	float af=(float) (1e5*mw/Rs*dd/TK);  /* Absolute Feuchte in g/m^3 */
        
        if(pressure>dd) {
                /* Absolute virtual temperature */
        	float tv=(float)(T + (float)273.15)/((float)1-(float)0.379*dd/pressure)-(float)273.15;
        	
        	float vpz=dd/pressure*100;   /* Wasserpartialdruck Volumenprozent*/
        	zeile1.setText("TV: "+t2fs(tv)+" ; H2O: "+Math.round(vpz*10)/10.0+" Vol%");
        } else zeile1.setText("TV: -- ; H2O: -- Vol%");

        /* Now calculate the wet bulb temperature.*/

        if(dd>0) {
          double gamma=0.00066*pressure*0.1;
          double delta=4098.0*dd*0.1/(td+237.3)/(td+237.3);
          double twb=(gamma*T+delta*td)/(gamma+delta);    	
          zeile2.setText("WB: "+t2fs(twb));
        } else {
          zeile2.setText("WB: --");
        }
        zeile3.setText("SDD: "+Math.round(sdd*10)/10.0+" hPa");
    	taupunkt.setText(abk+": "+t2fs(td));
    	
    	if(mexpert==0 || pressure<=dd) wasser.setText("H2O: "+Math.round(af*10)/10.0+" g/m³");	
    	else {
    		float w=(float)0.62197*dd/(pressure-dd)*(float)1000.0; /* Wassergehalt/trockener Luft*/
    		wasser.setText("H2O: "+Math.round(af*10)/10.0+" g/m³ ; "+Math.round(w*10)/10.0+" g/kg");	
    	}
    }
}
