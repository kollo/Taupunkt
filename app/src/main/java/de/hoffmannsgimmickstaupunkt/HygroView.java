package de.hoffmannsgimmickstaupunkt;

/* HygroView.java (c) 2008-2015 by Markus Hoffmann 
 *
 * This file is part of Taupunkt / Dew Point for Android 
 * ==================================================================
 * Taupunkt / Dew Point for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class HygroView extends View  implements OnTouchListener  {
    
	private Paint paint;
	  
	private int textHeight;
	private int bx,by,bw,bh;
	private int sx,sy,sw,sh;
	private float tempmin=30, tempmax=-10;
	private float tmin=-10, tmax=45;
	private int tendenz;
	public float temperature;
	
	public void setTemperature(float t) {
		if(t-temperature>0) tendenz=1;
		else if(t-temperature<0) tendenz=-1;
		else tendenz=0;
		if(t>=0) temperature=t; /* Is hygrometer, so >0% */
		else temperature=0;
		if(temperature>tempmax) tempmax=temperature;
    	if(temperature<tempmin) tempmin=temperature;
	}
	public void setMinmax(float t1,float t2) {
		 tmin=t1;
		 tmax=t2;
	}
	public void resetMinmax(float t1,float t2) {
		 tempmin=tmax;
		 tempmax=tmin;
	}
    public HygroView(Context context) {
        super(context);
        initHygroView();        
    }
    public HygroView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initHygroView();  
      }
    public HygroView(Context context, AttributeSet attrs, int defaultStyle) {
        super(context, attrs, defaultStyle);
        initHygroView();  
      }
    protected void initHygroView() {
    	setFocusable(true); //necessary for getting the touch events
        setFocusableInTouchMode(true);
        this.setOnTouchListener(this);
       Resources r = this.getResources();
        paint=new Paint();
      //  paint.setStrokeWidth(1);
      //  paint.setStyle(Paint.Style.STROKE);
    	paint.setAntiAlias(false);
        paint.setStyle(Style.FILL_AND_STROKE);
        paint.setColor(Color.BLUE);
        paint.setTextSize(20);
      }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    	int modex=MeasureSpec.getMode(widthMeasureSpec);
    	int sizex=MeasureSpec.getSize(widthMeasureSpec);
    	int modey=MeasureSpec.getMode(heightMeasureSpec);
    	int sizey=MeasureSpec.getSize(heightMeasureSpec);
    	if (modex == MeasureSpec.UNSPECIFIED) { 
    		sw=200;
    	} else {
    		sw=sizex;
    	}
    	if (modey == MeasureSpec.UNSPECIFIED) { 
    		sh=280;
    	} else {
    		sh=sizey;
    	}
    	setMeasuredDimension(sw, sh);
    }
  	private int kt(double dut) {
		  return by+bh-(int)((dut-tmin)/(tmax-tmin)*bh);
	}

    
    @Override
    public void onDraw(Canvas canvas) {
    	sw=getMeasuredWidth();
        sh=getMeasuredHeight();
        bw=Math.min(sw,sh/2);
        float tf=(float) (sw/120.0);
        bh=sh-7-13-14;
        by=sy+7;
        bx=sx+(sw-bw)/2;
        
        canvas.drawColor(Color.BLACK);
        paint.setTextSize(16*tf);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.WHITE);
      //  paint.setAntiAlias(true);
        canvas.drawCircle(bx+bw/2,by,6, paint);
        canvas.drawCircle(bx+bw/2,sy+sh-14,14, paint);
      //  paint.setAntiAlias(false);
        canvas.drawLine(bx+bw/2-6,kt(tmin),bx+bw/2-6,kt(tmax), paint);
        canvas.drawLine(bx+bw/2+6,kt(tmin),bx+bw/2+6,kt(tmax), paint);
               
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        
        paint.setColor(Color.BLACK);
        canvas.drawRect(bx+bw/2-5,by,bx+bw/2+6,by+bh, paint);
        
        if(temperature<0) paint.setColor(Color.BLUE);
        else paint.setColor(Color.GREEN);
        
        canvas.drawCircle(bx+bw/2,sy+sh-14,13, paint);
        canvas.drawRect(bx+bw/2-5,kt(temperature),bx+bw/2+6,by+bh, paint);
  	  paint.setColor(Color.WHITE);

  	  canvas.drawLine(bx+bw/2-6,kt(tempmin),bx+bw/2+7,kt(tempmin), paint);
  	  canvas.drawLine(bx+bw/2-6,kt(tempmax),bx+bw/2+7,kt(tempmax), paint);
  	  if(Math.signum(tmin)!=Math.signum(tmax)) canvas.drawLine(bx+bw/2-10,kt(0),bx+bw/2+12,kt(0), paint);

  	  canvas.drawLine(bx+bw/2+20,by,bx+bw/2+20,by+bh, paint);
  	  float tstep=1;
  	  String s;
  	  paint.setTextSize(6*tf);
  	  for(float i=Math.max(0,tmin); i<=tmax*1.01;i+=tstep) {
  		  if((int)i%10==0) {
  			  canvas.drawLine(bx+bw/2+20,kt(i),bx+bw/2+20+6,kt(i), paint);
  			  s=String.format("%d",(int) i);
  			  canvas.drawText(s,bx+bw/2+20+8,kt(i)+3*tf,paint);
  		  }
  		  else if((int)i%5==0) canvas.drawLine(bx+bw/2+20,kt(i),bx+bw/2+20+4,kt(i), paint);
  		  else canvas.drawLine(bx+bw/2+20,kt(i),bx+bw/2+20+2,kt(i), paint);
  	  }
  	  for(float i=Math.min(0,tmax); i>=tmin;i-=tstep) {
 		  if((int)i%10==0) {
 			  canvas.drawLine(bx+bw/2+20,kt(i),bx+bw/2+20+6,kt(i), paint);
  			  s=String.format("%d",(int) i);
  			  canvas.drawText(s,bx+bw/2+20+8,kt(i)+3*tf,paint);
 		  }
  		  else if((int)i%5==0) canvas.drawLine(bx+bw/2+20,kt(i),bx+bw/2+20+4,kt(i), paint);
  		  else canvas.drawLine(bx+bw/2+20,kt(i),bx+bw/2+20+2,kt(i), paint);
 	  
  	  }
  	paint.setAntiAlias(true);
  	paint.setTextSize(16*tf);
  	s=""+Math.round(temperature*10)/10.0+"%";
  	canvas.drawText(s,bx+bw/2-8-paint.measureText(s),by+bh/2,paint);
  	paint.setAntiAlias(false);
  }
    
   
    
 public boolean onTouch(View view, MotionEvent event) {
   float nt=tmin+(by+bh-event.getY())/bh*(tmax-tmin);
   if(nt>tmax) nt=tmax;
      setTemperature(nt);
      invalidate();
      return true;
   }
    
}
