Guide to contributing to Taupunkt / Dew Point for Android
==========================================================

The App is currently also in the Google Play Store, and has been there since
2011. I try to maintain the version there for as long as I have time to do it.


I have stopped development on this app myself, because I think it is quite
complete. If someone wants to further contribute I suggest following
improvements:

1. Translate the included help/documentation of all settings into more languages.
2. Improve the expert mode

If you find bugs please post your issues here. 
This repository here is the right place to do so.

## License and attribution

All contributions must be properly licensed and attributed. If you are
contributing your own original work, then you are offering it under a CC-BY
license (Creative Commons Attribution). If it is code, you are offering it under
the GPL-v2. You are responsible for adding your own name or pseudonym in the
Acknowledgments file, as attribution for your contribution.

If you are sourcing a contribution from somewhere else, it must carry a
compatible license. The project was initially released under the GNU public
licence GPL-v2 which means that contributions must be licensed under open
licenses such as MIT, CC0, CC-BY, etc. You need to indicate the original source
and original license, by including a comment above your contribution. 

## Contributing with a Pull Request

The best way to contribute to this project is by making a pull/merge-request:

1. Login with your codeberg account or create one now.
2. [Fork](https://codeberg.org/kollo/Taupunkt.git) the Dew Point/Taupunkt for Android repository. 
Work on your fork.
3. Create a new branch on which to make your change, e.g.
`git checkout -b my_code_contribution`, or make the change on the `new` branch.
4. Edit the file where you want to make a change or create a new file in the `contrib` directory if you're not sure where your contribution might fit.
5. Edit `ACKNOWLEGEMENTS` and add your own name to the list of contributors under the section with the current year. Use your name, or a codeberg ID, or a pseudonym.
6. Commit your change. Include a commit message describing the correction.
7. Submit a pull-request against the Taupunkt for Android repository.

## Contributing with an Issue

If you find a mistake and you're not sure how to fix it, or you don't know how
to do a pull/merge-request, then you can file an Issue. Filing an Issue will help us
see the problem and fix it.

Create a [new Issue](https://codeberg.org/kollo/Taupunkt/issues/new?issue) now!


## Thanks

Thanks to Bernd Kuemmel, IMFUFA, Roskilde University Centre, 
PB 260, DK-4000 Roskilde 
for explaining the Dew Point formula on this page:

http://www.faqs.org/faqs/meteorology/temp-dewpoint/

